rails new iFood

rails g controller Restaurants
rails g controller Foods

rails g model Category description
rails g model Food description image_url value:float
rails g model Restaurant description
rails g model Locale city state street number:integer

rails g migration AddRestaurantToLocale restaurant:references
rails g migration AddRestaurantToFood restaurant:references
rails g migration AddCategoryToFood category:references

---------------------------------------------------------------------------
-> Adicionado usuário e ligado ao endereço e restaurante (se houver)
rails g controller User
rails g model User description admin:boolean email password
rails g migration AddUserToLocale user:references
rails g migration AddUserToRestaurant user:references
rails g migration AddPasswordToUser password:string

----------------------------------------------------------------------------

-> Adicionando possibilidade de adicionar comidas ao carrinho
rails g controller CartItems
rails g model CartItem quantity:integer

rails g migration AddUserToCartItem user:references
rails g migration AddFoodToCartItem food:references

----------------------------------------------------------------------------
