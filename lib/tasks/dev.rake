namespace :dev do
  desc 'Configura ambiente de desenvolvimento'
  task setup: :environment do
    if Rails.env.development?
      %x(rails db:drop db:create db:migrate db:seed)
    end
  end
end
