Rails.application.routes.draw do
  root 'foods#index'

  get '/foods', to: 'foods#index'
  get '/foods/new', to: 'foods#new'
  post '/foods', to: 'foods#create'
  delete '/foods', to: 'foods#destroy'

  get '/restaurants/new', to: 'restaurants#new'
  post '/restaurants', to: 'restaurants#create'

  get '/sign_in', to: 'sessions#new'
  post '/sign_in', to: 'sessions#create'
  delete '/sign_out', to: 'sessions#destroy'

  get '/users', to: 'users#new'
  post '/users', to: 'users#create'

  get '/cart_list', to: 'cart_items#show'
  post '/buy', to: 'cart_items#create'
  patch '/add_cart', to: 'cart_items#update'
  delete '/cart_item', to: 'cart_items#destroy'
end
