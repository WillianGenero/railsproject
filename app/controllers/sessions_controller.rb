class SessionsController < ApplicationController
  def create
    @user = User.find_by(email: session_params[:email])

    if @user && @user.password == session_params[:password]
      helpers.sign_in
      redirect_to foods_path
    else
      respond_to do |format|
        format.html { render :new }
      end
    end
  end

  def destroy
    helpers.sign_out
    redirect_to foods_path
  end

  private
  def session_params
    params.require(:session).permit(:email, :password)
  end
end
