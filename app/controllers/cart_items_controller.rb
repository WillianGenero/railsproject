class CartItemsController < ApplicationController
  before_action :set_cart_item, only: [:update, :destroy]

  def show
    @cart_items = CartItem.includes(:food)
    @cart_items = CartItem.where(user_id: helpers.current_user.id)

    @total_quantity = @cart_items.map {|cart_item| cart_item[:quantity]}.reduce(:+)
    #@total_value = @cart_items.map {|cart_item| (cart_item.dig :food, :value)}.reduce(:+)

    @total_value = 0.00
    @cart_items.each do |cart_item|
      @total_value += (cart_item.food.value * cart_item.quantity)
    end
  end

  def create
    @item = CartItem.new(cart_items_params)
    if @item.save!
      redirect_to foods_path
    end
  end

  def update
    if @cart_item.update!(cart_items_params)
      redirect_to cart_list_path
    end
  end

  def destroy
    @cart_item.destroy
    redirect_to cart_list_path
  end


  private
    def set_cart_item
      @cart_item = CartItem.find(params[:id])
    end

    def cart_items_params
      params.require(:cart_items).permit(:food_id, :user_id, :quantity)
    end
end
