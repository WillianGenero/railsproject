class UsersController < ApplicationController
  def new
    @user = User.new
    @user.build_locale
  end

  def create
    @user = User.new(user_params)
    if @user.save
      helpers.sign_in
      redirect_to foods_path
    end
  end

  private
    def user_params
      params.require(:user).permit(:admin, :description, :email, :password, locale_attributes: [:id, :city, :number, :state, :street])
    end
end
