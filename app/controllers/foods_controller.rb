class FoodsController < ApplicationController
  before_action :get_categories, only: [:index, :new]
  before_action :set_food, only: [:destroy]

  def index
    @foods = Food.includes(restaurant: :locale)

    if helpers.logged_in? && !helpers.current_user.restaurant.nil?
      @foods = Food.of_restaurant(helpers.current_user.restaurant.id)
    end

    if params[:category_id]
      @foods = @foods.of_category(params[:category_id])
    end

    if helpers.logged_in?
      @cart_items = CartItem.where(user_id: helpers.current_user.id)
      @quantity_items = @cart_items.map {|cart_item| cart_item[:quantity]}.reduce(:+)
    end
  end

  def new
    @food = Food.new
  end

  def create
    @food = Food.new(food_params)
    if @food.save!
      redirect_to foods_path
    end
  end

  def destroy
    if @food.destroy!
      redirect_to foods_path
    end
  end

  private
    def food_params
      params.require(:food).permit(:description, :value, :image_url, :image, :restaurant_id, :category_id)
    end

    def get_categories
      @categories = Category.all
    end

    def set_food
      @food = Food.find(params[:id])
    end
end
