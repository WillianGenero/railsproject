class RestaurantsController < ApplicationController
  def new
    @restaurant = Restaurant.new
    @restaurant.build_locale
  end

  def create
    @restaurant = Restaurant.new(restaurant_params)
    if @restaurant.save
      redirect_to foods_path
    end
  end

  private
    def restaurant_params
      params.require(:restaurant).permit(:description, :user_id, locale_attributes: [:id, :city, :state, :street, :number])
    end
end
