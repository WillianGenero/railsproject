class User < ApplicationRecord
  has_one :restaurant, required: false

  has_many :cart_items
  has_many :foods, through: :cart_items

  has_one :locale
  accepts_nested_attributes_for :locale
end
