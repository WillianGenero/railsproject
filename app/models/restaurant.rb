class Restaurant < ApplicationRecord
  has_many :foods

  has_one :locale
  accepts_nested_attributes_for :locale

  belongs_to :user
end
