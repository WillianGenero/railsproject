class Food < ApplicationRecord
  belongs_to :category
  belongs_to :restaurant

  has_one_attached :image

  has_many :cart_items, dependent: :destroy
  has_many :users, through: :cart_items

  scope :of_category, -> (category_id) { where(category_id: category_id) }
  scope :of_restaurant, -> (restaurant_id) { where(restaurant_id: restaurant_id) }

  validate :acceptable_image

  def acceptable_image
    return unless image.attached?

    acceptables_types = ['image/jpeg', 'image/png']
    unless acceptables_types.include?(image.content_type)
      errors.add(:image, 'Arquivo deve ser no formado JPEG ou PNG')
    end
  end
end
