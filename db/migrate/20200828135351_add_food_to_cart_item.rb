class AddFoodToCartItem < ActiveRecord::Migration[6.0]
  def change
    add_reference :cart_items, :food, null: false, foreign_key: true
  end
end
