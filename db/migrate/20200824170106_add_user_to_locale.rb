class AddUserToLocale < ActiveRecord::Migration[6.0]
  def change
    add_reference :locales, :user, foreign_key: true
  end
end
