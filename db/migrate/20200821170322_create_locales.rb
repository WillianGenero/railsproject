class CreateLocales < ActiveRecord::Migration[6.0]
  def change
    create_table :locales do |t|
      t.string :state
      t.string :city
      t.string :street
      t.integer :number

      t.timestamps
    end
  end
end
