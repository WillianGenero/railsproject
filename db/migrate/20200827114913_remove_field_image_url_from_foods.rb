class RemoveFieldImageUrlFromFoods < ActiveRecord::Migration[6.0]
  def change
    remove_column :foods, :image_url, :string
  end
end
