class AddCategoryToFood < ActiveRecord::Migration[6.0]
  def change
    add_reference :foods, :category, null: false, foreign_key: true
  end
end
