class AddRestaurantToLocale < ActiveRecord::Migration[6.0]
  def change
    add_reference :locales, :restaurant, foreign_key: true
  end
end
